import express, { Router, Request } from 'express';
import admin from 'firebase-admin';
const PubSub = require(`@google-cloud/pubsub`);

admin.initializeApp({
  credential: admin.credential.applicationDefault()
});

const db = admin.firestore();

const pubsub = new PubSub();
const topicName = 'projects/ragnarok-db/topics/last-refines';

const router = Router()
router.get("/", async (req, res, next) => {
  try {
    const noteSnapshot = await db.collection("weapon-refine").get();
    const weaponRefinements = [];
    noteSnapshot.forEach(doc => {
      weaponRefinements.push({
        lvl: parseInt(doc.id),
        nome: doc.data().nome,
        quantidade: doc.data().quantidade
      });
    });

    const data = JSON.stringify(weaponRefinements);
    const dataBuffer = Buffer.from(data);

    pubsub
      .topic(topicName)
      .publisher()
      .publish(dataBuffer)
      .then(messageId => {
        console.log(
          `Todos os refinements foram solicitados e a menssagem de id: ${messageId} foi publicada.`
        );
      })
      .catch(err => {
        console.error("ERROR:", err);
      });

    res.json(weaponRefinements.sort((a, b) => a.lvl - b.lvl));
  } catch (e) {
    next(e);
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    if (!id) throw new Error("Entre com um nível de refinamento válido!");
    const weaponRefinement = await db
      .collection("weapon-refine")
      .doc(id)
      .get();
    if (!weaponRefinement.exists) {
      throw new Error("Esse nível de refinamento não existe!");
    }

    const data = JSON.stringify({
      lvl: parseInt(weaponRefinement.id),
      nome: weaponRefinement.data().nome,
      quantidade: weaponRefinement.data().quantidade
    });
    const dataBuffer = Buffer.from(data);

    pubsub
      .topic(topicName)
      .publisher()
      .publish(dataBuffer)
      .then(messageId => {
        console.log(
          `O refinement de lvl ${weaponRefinement.id} foi solicitado e a menssagem de id: ${messageId} foi publicada.`
        );
      })
      .catch(err => {
        console.error("ERROR:", err);
      });

    res.json({
      lvl: parseInt(weaponRefinement.id),
      nome: weaponRefinement.data().nome,
      quantidade: weaponRefinement.data().quantidade
    });
  } catch (e) {
    next(e);
  }
});


export default router;