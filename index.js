import app from "./rag-api";

function App(req, res) {
    if (!req.url) {
        req.url = '/';
        req.path = '/';
    }
    return app(req, res);
}

const weaponRefine = App;

export { weaponRefine }